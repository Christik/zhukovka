$(document).ready(function(){


	/* ==========================================================================
	 Screen Map Animation
	 ========================================================================== */

	scrollAnim($('.scr-map__txt'),$('.scr-map__txt'),$(window).height()/3, true);
	scrollAnim($('.celebrities'),$('.celebrities'),$(window).height()/3, true);

	$('.scr-map').each(function(){
		scrollAnim($(this),$(this),$(window).height()*0.7, true);
	});

	$(window).scroll(function(){
		if ($(window).scrollTop() > $(window).height()*0.75) {
			$('.beetle.main').css('transition-delay','0s');
			$('.beetle.main').addClass('animated');
		} else {
			$('.beetle.main').removeClass('animated');
		}
	});


	/* ==========================================================================
	 Counter
	 ========================================================================== */

	// Counter 1
	var max_number = 4500;

	var padding_zeros = '';
	for(var i = 0, l = max_number.toString().length; i < l; i++) {
		padding_zeros += '0';
	}

	var padded_now, numberStep = function(now, tween) {
		var target = $(tween.elem),
				rounded_now = Math.round(now);

		var rounded_now_string = rounded_now.toString()
		padded_now = padding_zeros + rounded_now_string;
		padded_now = padded_now.substring(rounded_now_string.length);

		target.prop('number', rounded_now).text(padded_now);
	};

	// Counter 2
	var max_number2 = 96;

	var padding_zeros2 = '';
	for(var i = 0, l = max_number2.toString().length; i < l; i++) {
		padding_zeros2 += '0';
	}

	var padded_now2, numberStep2 = function(now, tween) {
		var target = $(tween.elem),
				rounded_now = Math.round(now);

		var rounded_now_string = rounded_now.toString()
		padded_now = padding_zeros2 + rounded_now_string;
		padded_now = padded_now.substring(rounded_now_string.length);

		target.prop('number', rounded_now).text(padded_now);
	};

	var countCatalog =  $('.scr-cat__counter__num'),
		countFlag1 = true,
		countStamp = $('.scr-hot__logos .stamp'),
		countFlag2 = true;

	$(document).scroll(function(){
		if ( countFlag1 && $(window).scrollTop() >= countCatalog.offset().top - ($(window).height()*2)/3 ) {

			countCatalog.prop('number', 1000).animateNumber({
				number: max_number,
				numberStep: numberStep
			}, 3000, 'swing');

			countFlag1 = false;
		}

		if ( countFlag2 && $(window).scrollTop() >= countStamp.offset().top - ($(window).height()*2)/3 ) {

			countStamp.animateNumber({
				number: max_number2,
				numberStep: numberStep2
			}, 3000, 'swing');

			countFlag2 = false;
		}
	});


	/* ==========================================================================
	 People Animation
	 ========================================================================== */

	$('.scr-cat__head__people, .catalog__item__people, .scr-infra__about__pic .people, .propose-card__people').each(function(){
		scrollAnim($(this),$(this),$(window).height()/3, true);
	});


	/* ==========================================================================
	 Parallax Background
	 ========================================================================== */

	var parHead = $('.parallax-head'),
		winHeight = $(window).outerHeight();

	$(window).on('scroll', function () {
		var scroll = $(window).scrollTop(),
			coef = (scroll - parHead.offset().top + winHeight) / (winHeight);

		if ( scroll >= parHead.offset().top - winHeight ) {
			parHead.css({
				'background-position' : '50% ' + Math.floor(coef * 100) + '%'
			});

			if ( scroll >= parHead.offset().top ) {
				parHead.css({
					'background-position' : '50% 100%'
				});
			}
		}
	});

	/* ==========================================================================
	 Lists Animation
	 ========================================================================== */

	$('.scr-infra__list').each(function(){
		scrollAnim($(this),$(this),$(window).height()/3, true);
	});

	$('.scr-infra__list__more').each(function(){
		scrollAnim($(this),$(this),$(window).height()/3, true);
	});


	/* ==========================================================================
	 Cards Animation
	 ========================================================================== */

	$('.scr-cards__grid').each(function(){
		scrollAnim($(this),$(this),$(window).height()/2, true);
	});

	$('.card.card_wide').each(function(){
		scrollAnim($(this),$(this),$(window).height()/3, true);
	});


	/* ==========================================================================
	 Boss Animation
	 ========================================================================== */

	$('.scr-boss').each(function(){
		scrollAnim($(this),$(this),$(window).height()/2, false);
	});



	/*  ========================================================================== */

});









