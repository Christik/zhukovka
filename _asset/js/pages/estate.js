$(document).ready(function(){


	/* ==========================================================================
	 Slider
	 ========================================================================== */

	var estateSlider = new Swiper ('.est-slider', {
		loop: true,
		slidesPerView: 1,
		navigation: {
			nextEl: '.slick-next',
			prevEl: '.slick-prev',
		},
		breakpoints: {
			// when window width is >= 320px
			999: {
				slidesPerView: 4,
			}
		}
	});

	$.fancybox.defaults.backFocus = false;


	$('.est-slider2').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		prevArrow: '<button type="button" class="slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-next"><span>след.</span></button>',
		touchThreshold: 150,
		responsive: [
			{
				breakpoint: 998,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					prevArrow: '<button type="button" class="slick-prev"></button>',
					nextArrow: '<button type="button" class="slick-next"><span>след.</span></button>',
					touchThreshold: 150
				}
			}
		]
	});


	/* ==========================================================================
	 People Animation
	 ========================================================================== */

	$('.txt-box__people').each(function(){
		scrollAnim($(this),$(this),$(window).height()/3, true);
	});


	/*  ========================================================================== */

});









