/* ==========================================================================
 Preloader
 ========================================================================== */

$(window).on('load', function () {
	$('body').delay(600).addClass('loaded');
});


$(document).ready(function(){


	/* ==========================================================================
	 Switch Cadres
	 ========================================================================== */

	var i = 1,
		totalSlides = 20,
		s = 70;

	function myInterval(){
		var count = i;

		if (i < 10) count = '0' + i;

		if (i == totalSlides) i = 1;
		else i++;

		$('.beetle img').attr("src", "_asset/img/beetle/Beetle_000" + count + "-min.png");

		setTimeout(myInterval, s);
	}
	setTimeout(myInterval ,s);


	/* ==========================================================================
	 Hamburger Menu
	 ========================================================================== */

	$('.burger').click(function(){
		var burger = $(this),
				classAct = 'is-active',
				classOpn = 'menu-opened';

		if ( !burger.hasClass(classAct) ) {
			burger.addClass(classAct);
			$('body').addClass(classOpn);
		} else {
			burger.removeClass(classAct);
			$('body').removeClass(classOpn)
		}
	});


	/* ==========================================================================
	 Fancybox
	 ========================================================================== */

	$("[data-fancybox]").fancybox({
		buttons : ['close'],
		youtube : {
			controls : 1,
			showinfo : 0
		},
		vimeo : {
			color : '45b7fe'
		}
	});



	/* ==========================================================================
	 Image Cover
	 ========================================================================== */

	$('.js-bg-cover').each(function(){
		var item = $(this),
			img = item.find('.js-bg-cover__img');

		item.css('background-image','url(' + img.attr('src') + ')');
	});


	/* ==========================================================================
	 Buttons
	 ========================================================================== */

	function convertPlus (item) {
		if ( item < 0 ) {
			return item*(-1);
		} else {
			return item;
		}
	}

	$('.js-to').click(function(e){
		e.preventDefault();

		var link = $(this),
			target = $(this).data('target'),
			href = $(this).attr('href');

		if ( typeof href !== typeof undefined && href !== false ) {
			target = href;
		}

		if ( $('body').hasClass('menu-opened') ) {
			$('.burger').removeClass('is-active');
			$('body').removeClass('menu-opened');
		}

		$('body,html').animate({
			scrollTop: $(target).offset().top
		}, ((convertPlus($(target).offset().top - link.offset().top))/$(document).outerHeight() * 4000), 'linear');

	});


	/* ==========================================================================
	 Hot Slider
	 ========================================================================== */

	$('.hot-card__gall').each(function(){
		var sFor = $(this).find('.hot-card__gall__for'),
			sNav = $(this).find('.hot-card__gall__nav');

		sFor.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			asNavFor: '#' + sNav.attr('id'),
			touchThreshold: 150
		});

		sNav.slick({
			slidesToShow: 6,
			slidesToScroll: 1,
			arrows: false,
			asNavFor: '#' + sFor.attr('id'),
			focusOnSelect: true,
			variableWidth: true
		});
	});

	$('.hot-slider').each(function(){
		$(this).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			touchThreshold: 150,
			variableWidth: true,
			prevArrow: '<button type="button" class="slick-prev"></button>',
			nextArrow: '<button type="button" class="slick-next"><span>след.</span></button>'
		});
	});


	/* ==========================================================================
	 Read More
	 ========================================================================== */

	$(".read-more").each(function(){

		var readMore = $(this),
			parent = readMore.closest('.typical-txt'),
			txtCont = readMore.html();

		if ( readMore.outerHeight() < parent.outerHeight() ) {

			readMore.find('.js-more').remove();

		} else {

			setTimeout(function(){

				readMore.dotdotdot({
					ellipsis: '... ',
					after: "span.js-more"
				}).find('.js-more').click(function(){

					$('.modal-more .typical-txt').html(txtCont);
					$('.modal-more').addClass('opened');

				});

			}, 500);

		}

	});

	// close more modal
	$('.js-more-close').click(function(){
		$('.modal-more').removeClass('opened');
	});


	/* ==========================================================================
	 Hypothec
	 ========================================================================== */

	// Стоимость

	var rangeCost = $('.js-range-cost'),
		outCost = $('.js-val-cost'),
		valCost = 0;

	rangeCost.ionRangeSlider({
		min: 50000000,
		max: 2000000000,
		from: 100000000,
		step: 100000,
		onStart: function(data){
			valCost = data.from;

			outCost.text(valCost.toLocaleString()); // вывод значения стоимости
		},
		onChange: function(data){
			valCost = data.from;

			outCost.text(valCost.toLocaleString()); // вывод значения стоимости

			firstPercent(valCost,valFirst); // вывод процента первого взноса
			bankCard(); // перерасчет в карточках банка
		}
	});


	// Первоначальный взнос

	var rangeFirst = $('.js-range-first'),
		outFirst = $('.js-val-first'),
		valFirst = 0;

	rangeFirst.ionRangeSlider({
		min: 50000000,
		max: 1800000000,
		from: 50000000,
		step: 1000,
		onStart: function(data){
			valFirst = data.from;

			outFirst.text(valFirst.toLocaleString()); // вывод первого взноса
		},
		onChange: function(data){
			valFirst = data.from;

			outFirst.text(valFirst.toLocaleString()); // вывод первого взноса

			firstPercent(valCost,valFirst); // вывод процента первого взноса
			bankCard(); // перерасчет в карточках банка
		}
	});


	// Срок

	var rangeTime = $('.js-range-time'),
		outTime = $('.js-val-time'),
		valTime = 0;

	rangeTime.ionRangeSlider({
		min: 1,
		max: 30,
		from: 10,
		step: 1,
		onStart: function(data){
			valTime = data.from;

			outTime.text(valTime.toLocaleString()); // вывод первого взноса
		},
		onChange: function(data){
			valTime = data.from;

			outTime.text(valTime.toLocaleString()); // вывод первого взноса

			bankCard(); // перерасчет в карточках банка
		}
	});


	// Процент первоначального взноса

	var outFirstPerc = $('.js-first-perc');

	function firstPercent(cost, first) {
		var resultTemp = first/cost * 100,
			result = resultTemp.toFixed(2);

		outFirstPerc.text( result ); // вывод процента первого взноса

		if ( result > 90 ) {
			outFirstPerc.parent().addClass('error');
		} else {
			outFirstPerc.parent().removeClass('error');
		}
	}

	firstPercent(valCost,valFirst);


	// Карточки банков

	var bankClass = 'is-disabled';

	function bankCard() {
		$('.bank-card').each(function(){
			var bank = $(this),
				bankFirst = parseInt(bank.data('first'))/100, // первый взнос в дроби
				bankOutFirst = bank.find('.js-bank-first'), // хтмл куда выводим первый взнос
				bankFirstVal = valCost * bankFirst, // первый взнос в рублях
				bankTime = parseInt(bank.data('time')), // срок банка
				bankRate = parseFloat(bank.data('rate')),
				bankOutPay = bank.find('.js-bank-pay'); // ставка банка

			bankOutFirst.text( bankFirstVal.toLocaleString() ); // выводим в карточке банка значение первого взноса

			// если выбранный перывй взнос меньше, чем взнос банка
			if ( bankFirstVal > valFirst || bankTime < valTime || (valFirst/valCost * 100) > 90 ) {
				bank.addClass(bankClass);
			} else {
				bank.removeClass(bankClass);
			}

			// рассчет ежемесячного платежа
			var rateMonth = bankRate/100/12, // ежемесячный процент
				tmp = Math.pow( (1 + rateMonth), (bankTime * 12) ),
				payMonth = (valCost - valFirst) * rateMonth * tmp / (tmp - 1), // ежемесячный платеж
				payMontRound = payMonth.toFixed(0); // ежемесячный платеж округленный до целых

			// если первый взнос больше 90%
			if ( (valFirst/valCost * 100) > 90 ) {
				bankOutPay.text('–');
			} else {
				bankOutPay.text( parseInt(payMontRound).toLocaleString() + ' ⃏' ); // вывод ежемесячного платежа
			}


			console.log(jQuery.type(payMontRound));

		});
	}

	bankCard();


	/* ==========================================================================
	 Form Validation
	 ========================================================================== */

	$('.form-valid').each(function(){

		$(this).validate({
			errorElement: 'div',
			errorClass: 'error-field',
			onclick: false,
			onkeyup: false,
			focusInvalid: false,
			errorPlacement: function (error, element) {
				if (element.attr("type") == "checkbox") {
					error.insertAfter(element.next('label'));
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler: function (form) {
				$.ajax({
					type: "POST",
					url: "app.php",
					data: $(form).serialize(),
					beforeSend: function(data) {
						$(form).find('[type="submit"]').attr('disabled', 'disabled');
					},
					success: function () { },
					complete: function(data) {
						$(form).find('[type="submit"]').prop('disabled', false);
					}
				});
				return false;
			}
		});

	});


	/* ==========================================================================
	 Quiz Slider
	 ========================================================================== */

	var mySwiper = new Swiper ('.swiper-container', {
		// Optional parameters
		direction: 'horizontal',
		loop: false,
		simulateTouch: false
	});

	$('.swiper-slide').each(function(){
		var item = $(this),
			btn = item.find('.js-next-step'),
			checkbox = item.find('input[type=checkbox]'),
			check = item.find('input[type=checkbox][checked]'),
			name = checkbox.attr('name');

		btn.click(function(){
			console.log(check.length);

			if ( checkbox.length > 0 ) {
				if ( $('input[name=' + name + ']').is(':checked') ) {
					mySwiper.slideNext();
				}
			} else {
				mySwiper.slideNext();
			}
		});
	});


	/* ==========================================================================
	 Quiz Count
	 ========================================================================== */

	var outQuizCount = $('.js-quiz-count');

	$('input[name=cost]').change(function(){
		outQuizCount.text( $(this).data('count') );
	});



	/* ==========================================================================
	 Modal
	 ========================================================================== */

	// get scrollbar width
	function getScrollBarWidth () {
		var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
				widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
		$outer.remove();
		return 100 - widthWithScroll;
	}

	var scrollbarWidth = getScrollBarWidth();

	$('.js-modal').each(function(){
		var link = $(this),
				target = $(link.data('target')),
				btnClose = target.find('.js-modal-close'),
				bodyClass = 'modal-opened',
				modalClass = 'opened';

		link.click(function(e){
			e.preventDefault();

			$('body').addClass(bodyClass);
			$('body').css('margin-right', scrollbarWidth);
			target.addClass(modalClass);
		});

		btnClose.click(function(e){
			e.preventDefault();

			$('body').removeClass(bodyClass);
			$('body').css('margin-right', 0);
			target.removeClass(modalClass);

			target.find('form').trigger("reset");
			setTimeout(function(){
				mySwiper.slideTo(0);
			}, 500);
		});
	});


	/* ==========================================================================
	 Simplebar
	 ========================================================================== */

	// if ( $(window).width() >= 998 ) {
	// 	$('.hypothec__right').each(element, new SimpleBar());
	// }


	/*  ========================================================================== */

});


/* ==========================================================================
 Functions
 ========================================================================== */

var winHeight = $(window).height();

function scrollAnim(target, trigger, offset, reverse) {
	if ( target.length > 0 ) {
		$(window).scroll(function(){
			if ($(window).scrollTop() > trigger.offset().top - $(window).height() + offset) {
				target.addClass('animated');
			} else {
				if ( reverse ) {
					target.removeClass('animated');
				}
			}
		});
	}
}

function scrollParallax(obj, parent, ratio) {
	$(document).on('scroll', function() {
		obj.css('margin-top', ($(document).scrollTop() - parent.offset().top)/ratio + 'px');
	});
}

function mouseParallax(obj,y,x,offsetY,offsetX) {
	$('body').mousemove(function(e) {
		obj.css({
			'margin-top': offsetY - e.pageY/y + 'px ',
			'margin-left': offsetX - e.pageX/x + 'px'
		});
	});

}







